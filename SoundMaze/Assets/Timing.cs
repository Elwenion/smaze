﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timing : MonoBehaviour {
	public float countdown=240.0f;
	public Text text;
	public Text gameOvertext;
	private bool run = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(run){
			countdown -= Time.deltaTime;
		}


		string minutes = Mathf.Floor(countdown / 60).ToString("00");
		string seconds = (countdown % 60).ToString("00");

		text.text = "Time: " + minutes + ":" + seconds;
		if(countdown <= 0.0f)
		{
			gameOvertext.text = "Game Over";
			run=false;
			countdown = 0.0f;
			StartCoroutine(Restart());

		}
	}

	IEnumerator Restart(){
		yield return new WaitForSeconds (5);
		Application.LoadLevel (Application.loadedLevel);
	}
}
