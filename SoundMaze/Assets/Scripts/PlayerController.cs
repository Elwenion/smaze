﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private Rigidbody rb;
	public float speed;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}
	// Update is called once per frame
	void FixedUpdate () 
	{
	
		float moveHorizontal =  Input.GetAxis ("Horizontal");
		float moveHVertical =  Input.GetAxis ("Vertical") ;
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveHVertical);
		
			rb.AddForce (movement*speed);
	}
}
