﻿using UnityEngine;
using System.Collections;

public class Cameracontrol : MonoBehaviour {

	public GameObject player;
	private Vector3 offset;




	// avoiding the camera spinning arround the playerball 
	//but be attached to the player
	void Start () 
	{
		offset = transform.position - player.transform.position;
	}
	
	// camera aligned with the player object when updating the scene in each frame
	//not normal update because the player has to stop moving before the camera would follow

	void LateUpdate () 
	{
		transform.position = player.transform.position + offset;
	}
}
